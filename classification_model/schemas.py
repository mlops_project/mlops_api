from pydantic import BaseModel


class Iris(BaseModel):
    seplength: float
    sepwidth: float
    petlength: float
    petwidth: float
