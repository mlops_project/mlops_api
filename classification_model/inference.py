from typing import Dict, List
import joblib
import pandas as pd
import numpy as np
from .schemas import Iris
import json


def model_inference(model_path: str, input_dict:  Iris, class_names: List[str]):

    """model
    Run model inference with the given model

    :param model_path: Sklearn model path
    :param input_dict: input dict
    :param class_names: name of classes
    """

    model = joblib.load(model_path)
    key_for_dict = ['sepal length (cm)',  'sepal width (cm)',  'petal length (cm)',  'petal width (cm)']
    new_dict = dict(zip(key_for_dict, list(input_dict.dict().values())))
    df = pd.DataFrame.from_dict([new_dict])
    pred: np.array = model.predict(df)[0]
    response: str = class_names[int(pred)]

    return response
