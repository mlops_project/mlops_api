from typing import List
from fastapi import FastAPI
from fastapi_health import health
import pathlib
from .inference import model_inference
from .schemas import Iris
import os

path_to_model = pathlib.Path(__file__).parent.parent.resolve() / "model" / "best_model.pkl"


# check if model is loaded correctly
def is_model_loaded() -> bool:
    return path_to_model.exists()


# initializing the API, see https://fastapi.tiangolo.com/tutorial/first-steps/
app = FastAPI(root_path = os.getenv("root_path") or "")
app.add_api_route("/health", health([is_model_loaded]))


@app.post("/classify", status_code=200)
def classify_content(input_json: Iris):

    model_path: str = str(path_to_model)
    class_names: List[str] = ['setosa', 'versicolor', 'virginica']

    response_final = model_inference(
        model_path=model_path, input_dict=input_json, class_names=class_names
    )

    return response_final


@app.get("/test", status_code=200)
def test():
    return {"message": "ok"}
