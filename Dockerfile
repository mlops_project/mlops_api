FROM python:3.10.11-slim-buster


ENV TZ=UTC

ARG mlflow_s3
ENV MLFLOW_S3_ENDPOINT_URL=$mlflow_s3

ARG artifact_root
ENV ARTIFACT_ROOT=$artifact_root

ARG aws_acces_key
ENV AWS_ACCESS_KEY_ID=$aws_acces_key

ARG aws_secret_key
ENV AWS_SECRET_ACCESS_KEY=$aws_secret_key

ARG tracking_uri
ENV MLFLOW_TRACKING_URI=$tracking_uri

ARG model_id
ENV MODEL_ID=$model_id

RUN pip install --upgrade pip
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY ./run.py run.py
COPY ./load.py load.py
COPY ./classification_model /classification_model

RUN python load.py

ENTRYPOINT ["python", "run.py"]
