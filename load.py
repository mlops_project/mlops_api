from joblib import dump
import mlflow
import pathlib
import os


BASE_DIR = pathlib.Path(__file__).parent.resolve()
MODEL_DIR = BASE_DIR / "model"


def load_model():
	os.makedirs(MODEL_DIR, exist_ok=True)
	# storage = os.getenv("MLFLOW_S3_ENDPOINT_URL")
	tracking_uri = os.getenv("MLFLOW_TRACKING_URI")
	run_id = os.getenv("MODEL_ID")
	mlflow.set_tracking_uri(tracking_uri)
	loaded_model = mlflow.sklearn.load_model("runs:/" + run_id + "/model")
	dump(loaded_model, MODEL_DIR / "best_model.pkl")


if __name__ == '__main__':
	if not MODEL_DIR.exists():
		load_model()
