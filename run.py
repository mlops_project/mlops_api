import uvicorn
import os
import functools


uv_run = functools.partial(
	uvicorn.run,
	"classification_model.api:app",
	host="0.0.0.0",
	port=5000,
)

if __name__ == '__main__':
	root_path = os.getenv("root_path")
	if root_path is not None:
		uv_run(root_path=root_path)
	else:
		uv_run()
